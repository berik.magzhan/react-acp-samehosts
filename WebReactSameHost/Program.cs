using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebReactSameHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = CreateHostBuilder(args).Build();
            builder.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var iService = !(Debugger.IsAttached || args.Contains("--console"));
                    if(iService)
                    {
                        var processModule = Process.GetCurrentProcess().MainModule;
                        if(processModule != null)
                        {
                            config.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                            config.AddJsonFile("hostsettings.json", optional: true);
                            config.AddJsonFile("appsettings.json", optional: true);
                        }
                    }
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
